const gulp = require('gulp'),
      pug = require('gulp-pug'),
      using = require('gulp-using'),
      plumber = require('gulp-plumber'),
      clean = require('gulp-clean'),
      browserSync = require('browser-sync');

const reload = function(config) {
  return browserSync.reload({stream: true});
}

const pugSrc = './src/pages/**/*.pug';
const pugDist = './dist/';
const pugWatch = './src/**/*.pug';
const cleanFolder = './dist';

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: './dist/'
    }
  })
});


gulp.task('pug', function() {
  return gulp.src(pugSrc)
    .pipe(using({}))
    .pipe(plumber())
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest(pugDist))
    .pipe(reload())
});

gulp.task('watcher', function() {
  gulp.watch(pugWatch, (event, sb) => {
    gulp.start('pug');
  });
});

gulp.task('clean', function() {
  gulp.src(cleanFolder, { read: false })
    .pipe(clean({forse: true}))
});

gulp.task('default', ['watcher', 'browser-sync', 'pug']);